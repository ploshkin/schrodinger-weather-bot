import telebot
import datetime
import pymorphy2
import config
import json
import random
import py_ms_cognitive

from weather import Weather
from py_ms_cognitive import PyMsCognitiveImageSearch
from week_days import Days
from conditions import Conditions

py_ms_cognitive.PyMsCognitiveImageSearch.SEARCH_IMAGE_BASE = 'https://api.cognitive.microsoft.com/bing/v7.0/images/search'

morph = pymorphy2.MorphAnalyzer()

bot = telebot.TeleBot(config.telebot_token)


@bot.message_handler(commands=['help'])
def handle_start_help(message):
    respond = '''
Меня можно спршивать про погоду в городах. Например:
"Какая погода в Москве послезавтра?".

В ответ я пришлю прогноз, стишок и фотографию города.
'''
    bot.send_message(message.chat.id, respond)


@bot.message_handler(commands=['start'])
def handle_start_help(message):
    respond = 'Жду запрос!'
    bot.send_message(message.chat.id, respond)


class WeatherForecast(object):
    def __init__(self):
        self.weather = Weather()
        self.city = None
        self.city_rus = None
        self.region = None
        self.low = None
        self.high = None
        self.date = None
        self.condition = None
        self.condition_rus = None
        self.now = False
        self.wind = None
        self.pressure = None
        self.template_text = '''📍 {city}, {date:%d.%m.%y}:
{condition}
🌡 {low} .. {high} °C'''

    def __repr__(self):
        return self.template_text.format(
            city=self.city_rus,
            date=self.date,
            condition=self.condition_rus,
            low=self.low,
            high=self.high
        )

    def make_forecast(self, city, day_num, now=False):
        info = self.weather.lookup_by_location(city)

        location = info.location()
        self.city = location['city']
        self.region = location['region']

        self.city_rus = city.title()

        weather_info = info.forecast()[day_num]

        self.date = datetime.datetime.now() + datetime.timedelta(days=day_num)
        self.condition = weather_info.text()
        self.condition_rus = Conditions.translates[weather_info.text().lower()]
        self.high = fahrenheit2celsius(int(weather_info.high()))
        self.low = fahrenheit2celsius(int(weather_info.low()))


@bot.message_handler(content_types=['text'])
def echo_message(message):
    chat_id = message.chat.id
    result = parse_message(message.text.strip('?.,!'))

    if result['location'] is not None:
        try:
            forecast = WeatherForecast()
            forecast.make_forecast(result['location'], result['day'])

            bot.send_message(chat_id, repr(forecast))

            category = Conditions.categories[forecast.condition.lower()]
            bot.send_message(chat_id, choose_poem(category))

            image_url = get_image_url(forecast.city, forecast.condition)
            bot.send_photo(chat_id, image_url)
        except Exception as e:
            respond = '''Сорри, что-то пошло не так.'''
            bot.send_message(chat_id, respond)

    else:
        respond = '''Я ничего не понял.
Спроси чего попроще.'''
        bot.send_message(chat_id, respond)


def choose_poem(category):
    with open('poems.json', encoding='utf8') as poems_fp:
        poems = json.load(poems_fp)

    count = len(poems[category])
    poem_num = random.randrange(count)
    return poems[category][poem_num]


def parse_message(text):
    day = 0
    location = None
    words = text.split()
    for word in words:
        result = morph.parse(word)[0]
        word_normalized = result.normal_form
        if (word_normalized in Days.days or
            word_normalized in Days.week_days or
            word_normalized in Days.week_days_short):
            day = Days.get_days_offset(word_normalized)
        elif ((result.tag.case == 'nomn' or result.tag.case == 'loct' or
               result.tag.case == 'datv') and
              result.tag.POS == 'NOUN' and word_normalized != 'погода'):
            location = word_normalized

    return {'location': location, 'day': day}



def fahrenheit2celsius(temp):
    return int(round((temp - 32) * 5 / 9))


def get_image_url(location, condition):
    bing_image = PyMsCognitiveImageSearch(
        config.bing_token,
        '{} {}'.format(location, condition)
    )
    first_fifty_result = bing_image.search(limit=50, format='json')
    image_num = random.randrange(50)
    return first_fifty_result[image_num].content_url
