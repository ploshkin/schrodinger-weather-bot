import datetime


class Days(object):
    week_days = [
        'понедельник',
        'вторник',
        'среда',
        'четверг',
        'пятница',
        'суббота',
        'воскресенье'
    ]

    week_days_short = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']

    days = [
        'сегодня',
        'завтра',
        'послезавтра'
    ]

    def get_days_offset(day_text):
        cur_day = datetime.datetime.today().weekday()
        offset = 0

        for k in range(3):
            if day_text == Days.days[k]:
                offset = k
                return offset

        for k in range(7):
            if (day_text == Days.week_days[k] or
                day_text == Days.week_days_short[k]):
                offset = (k - cur_day) % 7
                return offset

        return offset
